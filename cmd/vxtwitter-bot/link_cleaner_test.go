package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestValidReplacementFixtures(t *testing.T) {
	for i, f := range validReplacementFixtures {
		clearStr, wasReplaced := cleanupTwitterLinks(f.input)
		require.Equalf(t, true, wasReplaced, "Case %d: expected text replacement: '%s'", i, f.input)
		assert.Equalf(t, f.expected, clearStr, "Case %d:\n- expected: %s\n- got     : %s\n", i, f.expected, clearStr)
	}
}

func TestNoReplacementFixtures(t *testing.T) {
	for i, f := range noReplacementFixtures {
		clearStr, wasReplaced := cleanupTwitterLinks(f.input)
		require.Equalf(t, false, wasReplaced, "Case %d: no replacement expected: '%s'", i, f.input)
		assert.Equalf(t, f.input, clearStr, "Case %d:\n- expected: %s\n- got     : %s\n", i, f.expected, clearStr)
	}
}

type fixture struct {
	input    string
	expected string
}

var (
	// These cases require a replacement to succeed.
	validReplacementFixtures = []fixture{
		{input: "twitter.com/random/path?param1=random&param2=param&s=19", expected: "vxtwitter.com/random/path"},
		{input: "x.com/random/path?param1=random&param2=param&s=19", expected: "vxtwitter.com/random/path"},
		{input: "https://twitter.com/random/path?param1=random&param2=param&s=19", expected: "https://vxtwitter.com/random/path"},
		{input: "https://x.com/random/path?param1=random&param2=param&s=19", expected: "https://vxtwitter.com/random/path"},

		{input: "Heading text with link twitter.com/random/path?param1=random&param2=param&s=19", expected: "Heading text with link vxtwitter.com/random/path"},
		{input: "Heading text with link x.com/random/path?param1=random&param2=param&s=19", expected: "Heading text with link vxtwitter.com/random/path"},
		{input: "Heading text with link https://twitter.com/random/path?param1=random&param2=param&s=19", expected: "Heading text with link https://vxtwitter.com/random/path"},
		{input: "Heading text with link https://x.com/random/path?param1=random&param2=param&s=19", expected: "Heading text with link https://vxtwitter.com/random/path"},

		{input: "Heading text with link twitter.com/random/path?param1=random&param2=param&s=19 and trailing text", expected: "Heading text with link vxtwitter.com/random/path and trailing text"},
		{input: "Heading text with link x.com/random/path?param1=random&param2=param&s=19 and trailing text", expected: "Heading text with link vxtwitter.com/random/path and trailing text"},
		{input: "Heading text with link https://twitter.com/random/path?param1=random&param2=param&s=19 and trailing text", expected: "Heading text with link https://vxtwitter.com/random/path and trailing text"},
		{input: "Heading text with link https://x.com/random/path?param1=random&param2=param&s=19 and trailing text", expected: "Heading text with link https://vxtwitter.com/random/path and trailing text"},

		{input: "Heading text with link twitter.com/random/path1?param1=random&param2=param&s=19 and trailing text with second link twitter.com/random/path2?param3=random&param4=param&s=20", expected: "Heading text with link vxtwitter.com/random/path1 and trailing text with second link vxtwitter.com/random/path2"},
		{input: "Heading text with link x.com/random/path1?param1=random&param2=param&s=19 and trailing text with second link x.com/random/path2?param3=random&param4=param&s=20", expected: "Heading text with link vxtwitter.com/random/path1 and trailing text with second link vxtwitter.com/random/path2"},
		{input: "Heading text with link twitter.com/random/path1?param1=random&param2=param&s=19 and trailing text with second link x.com/random/path2?param3=random&param4=param&s=20", expected: "Heading text with link vxtwitter.com/random/path1 and trailing text with second link vxtwitter.com/random/path2"},
		{input: "Heading text with link x.com/random/path1?param1=random&param2=param&s=19 and trailing text with second link twitter.com/random/path2?param3=random&param4=param&s=20", expected: "Heading text with link vxtwitter.com/random/path1 and trailing text with second link vxtwitter.com/random/path2"},
		{input: "Heading text with link https://twitter.com/random/path1?param1=random&param2=param&s=19 and trailing text with second link https://twitter.com/random/path2?param3=random&param4=param&s=20", expected: "Heading text with link https://vxtwitter.com/random/path1 and trailing text with second link https://vxtwitter.com/random/path2"},
		{input: "Heading text with link https://x.com/random/path1?param1=random&param2=param&s=19 and trailing text with second link https://x.com/random/path2?param3=random&param4=param&s=20", expected: "Heading text with link https://vxtwitter.com/random/path1 and trailing text with second link https://vxtwitter.com/random/path2"},
		{input: "Heading text with link https://twitter.com/random/path1?param1=random&param2=param&s=19 and trailing text with second link https://x.com/random/path2?param3=random&param4=param&s=20", expected: "Heading text with link https://vxtwitter.com/random/path1 and trailing text with second link https://vxtwitter.com/random/path2"},
		{input: "Heading text with link https://x.com/random/path1?param1=random&param2=param&s=19 and trailing text with second link https://twitter.com/random/path2?param3=random&param4=param&s=20", expected: "Heading text with link https://vxtwitter.com/random/path1 and trailing text with second link https://vxtwitter.com/random/path2"},
	}

	// No replacements should occur on these cases. No need to set the "expected" result because it must match the input. The testcase already takes this into account.
	noReplacementFixtures = []fixture{
		// No domain
		{input: "twitter .com"},
		{input: "x .com"},
		{input: "https://twitter .com"},
		{input: "https://x .com"},
		{input: "Heading text with no real link twitter. com with trailing text"},
		{input: "Heading text with no real link x. com with trailing text"},
		{input: "Heading text with no real link https://twitter .com with trailing text"},
		{input: "Heading text with no real link https://x .com with trailing text"},

		// Invalid domain, with or without query path
		{input: "twitter.org/?"},
		{input: "twitter.org/random/path?"},
		{input: "https://twitter.net/?"},
		{input: "https://twitter.net/random/path?"},
		{input: "Heading text with link twitter.org/?"},
		{input: "Heading text with link twitter.org/random/path?"},
		{input: "Heading text with link https://twitter.net/?"},
		{input: "Heading text with link https://twitter.net/random/path?"},
		{input: "x.org/?"},
		{input: "x.org/random/path?"},
		{input: "https://x.net/?"},
		{input: "https://x.net/random/path?"},
		{input: "Heading text with link x.org/?"},
		{input: "Heading text with link x.org/random/path?"},
		{input: "Heading text with link https://x.net/?"},
		{input: "Heading text with link https://x.net/random/path?"},

		// Valid domains with no query path
		{input: "twitter.com"},
		{input: "twitter.com/"},
		{input: "twitter.com/?"},
		{input: "x.com"},
		{input: "x.com/"},
		{input: "x.com/?"},
		{input: "https://twitter.com"},
		{input: "https://twitter.com/"},
		{input: "https://twitter.com/?"},
		{input: "https://x.com"},
		{input: "https://x.com/"},
		{input: "https://x.com/?"},
		{input: "Heading text with link twitter.com with trailing text"},
		{input: "Heading text with link twitter.com with trailing text/"},
		{input: "Heading text with link twitter.com with trailing text/?"},
		{input: "Heading text with link x.com with trailing text"},
		{input: "Heading text with link x.com with trailing text/"},
		{input: "Heading text with link x.com with trailing text/?"},
		{input: "Heading text with link https://twitter.com with trailing text"},
		{input: "Heading text with link https://twitter.com/ with trailing text"},
		{input: "Heading text with link https://twitter.com/? with trailing text"},
		{input: "Heading text with link https://x.com with trailing text"},
		{input: "Heading text with link https://x.com/ with trailing text"},
		{input: "Heading text with link https://x.com/? with trailing text"},

		// Already clean links
		{input: "vxtwitter.com"},
		{input: "vxtwitter.com/"},
		{input: "vxtwitter.com/?"},
		{input: "vxtwitter.com/random/path"},
		{input: "Heading text with link vxtwitter.com"},
		{input: "Heading text with link vxtwitter.com/"},
		{input: "Heading text with link vxtwitter.com/?"},
		{input: "Heading text with link vxtwitter.com/random/path"},
		{input: "Heading text with link vxtwitter.com with trailing text"},
		{input: "Heading text with link vxtwitter.com/ with trailing text"},
		{input: "Heading text with link vxtwitter.com/? with trailing text"},
		{input: "Heading text with link vxtwitter.com/random/path with trailing text"},
	}
)
