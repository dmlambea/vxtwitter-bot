package main

import (
	"errors"
	"flag"
	"fmt"
	"os"
)

const (
	minUpdatesTimeoutSeconds = 15
	maxUpdatesTimeoutSeconds = 150

	defUpdatesTimeoutSeconds = 30
)

type config struct {
	debug    bool
	telegram telegram
}

type telegram struct {
	token                 string
	updatesTimeoutSeconds int
}

// checkConfig performs a very basic check of the configuration parameters, returning an error if appropriate.
func checkConfig(cfg *config) (err error) {
	switch {
	case cfg.telegram.token == "":
		return errors.New("the Telegram auth token is required")
	case cfg.telegram.updatesTimeoutSeconds < minUpdatesTimeoutSeconds:
		return errors.New("timeout in seconds for updates is too low")
	case cfg.telegram.updatesTimeoutSeconds > maxUpdatesTimeoutSeconds:
		return errors.New("timeout in seconds for updates is too high")
	}
	return nil
}

// parseCmdLine processes the command line and returns a config struct with all the configured options.
func parseCmdLine() (cfg config) {
	flag.BoolVar(&cfg.debug, "debug", false, "enable debug logging")

	flag.StringVar(&cfg.telegram.token, "tg-token", "", "auth token")
	flag.IntVar(&cfg.telegram.updatesTimeoutSeconds, "tg-timeout", defUpdatesTimeoutSeconds, fmt.Sprintf("time in seconds to wait before asking for updates (min %d, max %d)", minUpdatesTimeoutSeconds, maxUpdatesTimeoutSeconds))

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [flags]\n\nwhere 'flags' are:\n", os.Args[0])
		flag.PrintDefaults()
		os.Exit(2)
	}
	flag.Parse()
	return cfg
}
