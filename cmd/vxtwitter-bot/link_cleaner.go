package main

import (
	"regexp"
)

var (
	// This 'twitterLinkRegexp' mess is configured to match as follows:
	//  - (?s):               sets '.' to match '\n' (kind of multiline matching).
	//  - (^|[[:space:]]):    matches the beginning of the line or any number of whitespace chars in the first capturing group.
	//  - (https://)?:        matches the optional 'https://' web link proto in the second capturing group.
	//  - (?:twitter|x)\.com: matches the valid Twitter domains for twitter.com or x.com in a non-capturing group.
	//  - (/[^\?[:space:]]+): matches the mandatory path up to the query separator in the third capturing group.
	//  - (\?[^[:space:]]*):  matches the entire query params in the fourth capturing group.
	//  - (.*)$:              matches up to the end of the input in the fifth capturing group.
	twitterLinkRegexp = regexp.MustCompile(`(?s)(^|[[:space:]])(https://)?(?:twitter|x)\.com(/[^\?[:space:]]+)(\?[^[:space:]]*)(.*)$`)

	vxTwitterReplaceString = "${1}${2}vxtwitter.com${3}${5}"
)

// cleanupTwitterLinks replaces every Twitter link iteratively because its regex is easier to maintain than
// one trying to replace all occurences in a single regexp.
func cleanupTwitterLinks(srcText string) (cleanText string, wasReplaced bool) {
	for {
		cleanText = twitterLinkRegexp.ReplaceAllString(srcText, vxTwitterReplaceString)
		if cleanText == srcText {
			// No replacements, all done
			return
		}
		wasReplaced = true
		srcText = cleanText
	}
}
