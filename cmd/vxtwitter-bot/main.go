package main

import (
	"log"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

func main() {
	cfg := parseCmdLine()
	if err := checkConfig(&cfg); err != nil {
		log.Fatal(err.Error())
	}

	// Create a bot and try to connect to Telegram
	bot, err := tgbotapi.NewBotAPI(cfg.telegram.token)
	if err != nil {
		log.Fatalf("error connecting to Telegram: %s", err)
	}

	// Telegram lib has its own logger so in case the desired level is debug or deeper,
	// the lib has to be instructed to issue its own logging.
	bot.Debug = cfg.debug

	log.Printf("Bot authorized to operate as %s", bot.Self.UserName)

	// Set initial offset to the beginning and start consuming events
	u := tgbotapi.NewUpdate(0)
	u.Timeout = cfg.telegram.updatesTimeoutSeconds
	updates := bot.GetUpdatesChan(u)
	for update := range updates {
		processUpdate(bot, update)
	}
}

// processUpdate takes care of a single message update. Messages without text content are ignored.
func processUpdate(bot *tgbotapi.BotAPI, update tgbotapi.Update) {
	if update.Message == nil {
		// Ignore empty message updates
		return
	}

	// Command messages are processed elsewhere
	if update.Message.IsCommand() {
		processCommand(bot, update.Message)
		return
	}

	// Create new text message with all Twitter links replaced
	newText, wasReplaced := cleanupTwitterLinks(update.Message.Text)
	if !wasReplaced {
		// If no replacement occured, simply stop processing
		return
	}

	// Try to delete the original message. If this operation fails, the processing ends.
	deleteMsg := tgbotapi.NewDeleteMessage(update.Message.Chat.ID, update.Message.MessageID)
	if resp, err := bot.Request(deleteMsg); err != nil || !resp.Ok {
		if err != nil {
			log.Printf("Error trying to execute delete operation: %s", err)
		} else {
			log.Printf("Unable to delete original message: error code: %d; error description: %s", resp.ErrorCode, resp.Description)
		}
		return
	}

	// Now, send the new message mentioning the author of the original, deleted one
	editedMessage := tgbotapi.NewMessage(update.Message.Chat.ID, "From @"+update.Message.From.UserName+":\n\n"+newText)
	_, err := bot.Send(editedMessage)
	if err != nil {
		log.Printf("Unable to send the new message: %s", err)
	}
}

func processCommand(bot *tgbotapi.BotAPI, incomingMessage *tgbotapi.Message) {
	sendFunc := func(msg string) bool {
		responseMsg := tgbotapi.NewMessage(incomingMessage.Chat.ID, msg)
		_, err := bot.Send(responseMsg)
		if err != nil {
			log.Printf("Unable to send command response message: %s", err)
			return false
		}
		return true
	}

	switch incomingMessage.Command() {
	case "help":
		for _, msg := range helpResponse {
			if ok := sendFunc(msg); !ok {
				return
			}
		}
	default:
		sendFunc("Unknown command. Please use '/help' if you need assistance.")
	}
}

var (
	helpResponse = []string{welcomeMessage, rationaleMessage, arenaMessage}

	welcomeMessage string = `Hi, I am vxTwitter Bot, at your service. :-)

You don't need to configure me or anything. I will be silently listening for messages containing (dirty) links to Twitter/X and, if I get one, I will replace it with a clean version of it. I clean links by replacing Twitter domains with the vxTwitter domain and by removing query param from the URL.`

	rationaleMessage string = `For the cleanup to work, please promote me to "admin" and grant me the "delete" permission. All other permissions can be revoked, I will not use them.

The "admin" mode is required for me to be able to read other people's messages in order to detect Twitter links. The "delete" permission is required for me to be able to replace the original message with the clean one.

The clean message will mention the original poster. No information is sent to anybody else nor stored anywhere.`

	arenaMessage = `Feel free to send me your Twitter links. I will privately replace them and send them back to you. This way you will soon become familiar with the way I work. :-)`
)
