# vxTwitter-bot

Twitter/X link cleaner for Telegram

**vxTwitter-bot** is designed to parse Twitter/X links and to replace them with vxTwitter links instead, while keeping the rest of the message intact. During the replacement, all query params in the Twitter/X links are removed, to improve privacy.

## Build instructions

Simply, check out the project, make sure you have a decent version of the Go compiler and execute the build the usual way in the project dir:

```sh
go build -o vxtwitter-bot ./cmd/vxtwitter-bot/...
```

